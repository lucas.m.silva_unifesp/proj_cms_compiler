# proj_cms_compiler


## Observações:
Este projeto foi desenvolvido na matéria de Compiladores da Universidade de São Paulo do campi de São José dos Campos, no ano de 2023.

Este projeto é uma atividade oriunda do livro "Compiladores princípios e práticas" de Kenneth C. Louden com algumas modificações pelo professor responsável, Luis Pereira. Além disso, o projeto se baseia na implementação do compilador para a linguagem TINY, desenvolvida pelo autor do livro.

## Iniciar o projeto:
Basta entrar na pasta src e passar o seguinte comando:
 
`sh script.sh`

Com isto o executável cms foi criado. Para compilar algum arquivo que seja escrito na linguagem C-, no terminal execute o executável cms junto do arquivo na linguagem C-, por exemplo:

`./cms ./sort.cms`

Para limpar as arquivos criados, use o comando:

`sh clear.sh`

## License
For open source projects, say how it is licensed.
