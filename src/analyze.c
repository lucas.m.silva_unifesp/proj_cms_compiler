/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
Criado po Lucas Moura da Silva
e baseado na linguagem TINY escrito por
Kenneth C. Louden
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

#include "analyze.h"

static void symtab_error(int line, char * name, char * message)
{
    printf("\n\nERRO SEMANTICO: ");
    if (name != NULL) {
        printf("%s -> ", name);
    }
    printf("%s", message);

    if (line > 0) {
        printf(" LINHA %d \n\n", line);
    } else {
        printf("\n\n");
    }

    exit(1);
}

/*
        Procediemnto que insere uma variavel na tabela de simbolos
*/
static void insert_node_variable(TreeNode * syntaxTree, ScopeList * scope)
{
        int hashValue;

        ScopeList * globalScope;

        // caso ainda nao tenha na tabela, entao trata como definicao nova
        if ((hashValue = lookup_element_symtab(scope, syntaxTree->atribute->name)) == -1) {
                globalScope = scope->parent;
                // caso nao exista uma funcao com o mesmo nome da variavel
                if ((globalScope == NULL) || !(lookup_element_symtab_nodeType(globalScope, syntaxTree->atribute->name, "Function") == 0)) {
                        if (syntaxTree->type == Integer) {
                                insert_element_symtab(scope, syntaxTree->atribute->name, syntaxTree->line, "Integer", "Variable", hashValue);
                        } else {
                                symtab_error(syntaxTree->line, syntaxTree->atribute->name, "Variavel declarada como void");
                        }
                } else {
                        // caso ecisa uma funcao com o mesmo nome da variavel, define como erro
                        symtab_error(syntaxTree->line, syntaxTree->atribute->name, "Nome da variavel ja declarada com funcao");
                }
        } else {
                // caso a variavel ja exista na tabela, define como erro
                symtab_error(syntaxTree->line, syntaxTree->atribute->name, "Variavel ja definida");
        }
}

/*
        Procediemnto que insere uma funcao na tabela de simbolos
*/
static void insert_node_function(TreeNode * syntaxTree, ScopeList * scope)
{
        int hashValue;

        // caso ainda nao tenha na tabela, entao trata como definicao nova
        if ((hashValue = lookup_element_symtab(scope, syntaxTree->atribute->name)) == -1) {
                insert_element_symtab(scope, syntaxTree->atribute->name, syntaxTree->line, syntaxTree->type==Integer?"Integer":"Void", "Function", hashValue);
        } else {
                // caso a funcao ja exista na tabela, define como erro
                symtab_error(syntaxTree->line, syntaxTree->atribute->name, "Funcao ja definida");
        }
}

/*
        Procediemnto que verifica se uma variavel ou funcao foi inserida na tabela de simbolos
*/
static void insert_node_declared(TreeNode * syntaxTree, ScopeList * scope, char * nodeType, char * declaredType, char * messageNotFind, char * messageNotDeclared)
{
        int hashValue;

        ScopeList * globalScope;

        // caso a funcao nao foi definida no escopo atual
        if ((hashValue = lookup_element_symtab(scope, syntaxTree->atribute->name)) == -1) {
                globalScope = scope->parent;
                // caso a funcao exista no escopo pai
                if (!(globalScope == NULL) && !(lookup_element_symtab_nodeType(globalScope, syntaxTree->atribute->name, declaredType) == 0)) {
                        if ((hashValue = lookup_element_symtab(globalScope, syntaxTree->atribute->name)) == -1) {
                                // caso a funcao nao exista em nenhum escopo, define como erro
                                symtab_error(syntaxTree->line, syntaxTree->atribute->name, messageNotFind);
                        } else {
                                insert_element_symtab(globalScope, syntaxTree->atribute->name, syntaxTree->line, "-", nodeType, hashValue);
                        }
                } else {
                        // caso a funcao nao exista, mas seu nome ja eh declarado com variavel, define como erro
                        symtab_error(syntaxTree->line, syntaxTree->atribute->name, messageNotDeclared);
                }
        } else {
                // caso ja tenha na tabela, entao ignorar localizacao e apenas acresentar numero da linha
                insert_element_symtab(scope, syntaxTree->atribute->name, syntaxTree->line, "-", nodeType, hashValue);
        }
}

static void insert_node_call(TreeNode * syntaxTree, ScopeList * scope)
{
        insert_node_declared(syntaxTree, scope, "Call", "Variable", "Funcao nao encontrada", "Nome da funcao ja declarada como variavel");
}

static void insert_node_id(TreeNode * syntaxTree, ScopeList * scope)
{
        insert_node_declared(syntaxTree, scope, "Identifier", "Function", "Variavel nao encontrada", "Nome da variavel ja declarada como funcao");
}

static void insert_node_array(TreeNode * syntaxTree, ScopeList * scope)
{
        insert_node_declared(syntaxTree, scope, "Array", "Function", "Variavel nao encontrada", "Nome da variavel ja declarada como funcao");
}

/*
        Procediemnto que insere um no da arvore sintatica na tabela de simbolos
*/
static void insert_node(TreeNode * syntaxTree, ScopeList * scope)
{
        int hashValue;

        ScopeList * globalScope;

        if (syntaxTree->nodeKind == StmtK) {
                switch (syntaxTree->kind->stmt) {
                        case VariableK:   
                                insert_node_variable(syntaxTree, scope);
                                break;
                        case FunctionK:   
                                insert_node_function(syntaxTree, scope);
                                break;
                        case CallK:       
                                insert_node_call(syntaxTree, scope);
                                break;
                        default:          
                                break;
                }
        } else {
                switch (syntaxTree->kind->exp) {
                        case IdK:
                                insert_node_id(syntaxTree, scope);
                                break;
                        case ArrayK:
                                insert_node_array(syntaxTree, scope);
                                break;
                        default:
                                break;
                }
        }
}

/*
        Procediemnto dos nos da arvore sintatica em pre-ordem
*/
static void insert(TreeNode * syntaxTree, ScopeList * scope)
{
    if (syntaxTree != NULL) {
        insert_node(syntaxTree, scope);

        int childIndex;

        if (syntaxTree->nodeKind == StmtK && syntaxTree->kind->stmt == FunctionK) {
                ScopeList * internScope; // escopo interno da funcao
                internScope = build_scope_list(syntaxTree->atribute->name, scope);

                if (scope->child == NULL) {
                        scope->child = internScope;
                } else {
                        ScopeList * sibling;
                        sibling = scope->child;

                        while (sibling->sibling != NULL)
                        sibling = sibling->sibling;
                        
                        sibling->sibling = internScope;
                }

                for (childIndex=0; childIndex<MAXCHILDREN; childIndex++) {
                        insert(syntaxTree->child[childIndex], internScope);
                }
        } else {
                for (childIndex=0; childIndex<MAXCHILDREN; childIndex++) {
                        insert(syntaxTree->child[childIndex], scope);
                }
        }

        insert(syntaxTree->sibling, scope);
    }

}

/*
        Procediemnto que checa os tipos dos nos-filhos da arvore sintatica
*/
static void check_node_children(ScopeList * scope, TreeNode * syntaxTree, int childIndex)
{
        int hashValue = lookup_element_symtab(scope, syntaxTree->child[childIndex]->atribute->name);

        ElementsSymTab * element;
        element = scope->hashTable[hashValue];

        while (strcmp(element->name, syntaxTree->child[childIndex]->atribute->name)) {
                element = element->next;
        }

        if (strcmp(element->type, "Integer")) {
                symtab_error(syntaxTree->line, element->name, "Funcao nao eh do tipo inteiro");
        }
}

/*
        Procediemnto que checa os nos com tipo ExpK para as operacoes e StmtK para as atribuicoes
*/
static void check_node(ScopeList * scope, TreeNode * syntaxTree)
{
        if (syntaxTree->nodeKind == ExpK) {
                if(syntaxTree->kind->exp == OpK) {
                        if (syntaxTree->child[0]->kind->stmt == CallK) {
                                check_node_children(scope, syntaxTree, 0);
                        } 
                        
                        if (syntaxTree->child[1]->kind->stmt == CallK) {
                                check_node_children(scope, syntaxTree, 1);
                        }

                        syntaxTree->type = syntaxTree->child[0]->type;
                
                }
        } else {
                if (syntaxTree->kind->stmt == AssignK) {
                        if(syntaxTree->child[1]->kind->stmt == CallK) {
                                check_node_children(scope, syntaxTree, 1);
                        }
                }
        }
}

/*
        Procediemnto que checa os tipos dos nos da arvore sintatica em in-ordem
*/
static void type_check_operands(ScopeList * scope, TreeNode * syntaxTree)
{
        if (syntaxTree != NULL) {
                int childIndex;

                for (childIndex=0; childIndex<MAXCHILDREN; childIndex++)
                        type_check_operands(scope, syntaxTree->child[childIndex]);

                check_node(scope, syntaxTree);

                type_check_operands(scope, syntaxTree->sibling);
        }
}


void build_symtab(TreeNode * syntaxTree)
{
        int hashValue;

        ScopeList * global;
        global = build_scope_list("global", NULL);

        /*
                Insere as funcoes input e output na tabela de simbolos antes de todas as outras
        */
        TreeNode * inputNode, * outputNode;

        inputNode = new_stmt_node(FunctionK, 0);
        inputNode->atribute->name = copy_string("input", 0);
        inputNode->type = Integer;

        outputNode = new_stmt_node(FunctionK, 0);
        outputNode->atribute->name = copy_string("output", 0);

        insert_node(inputNode, global);

        insert_node(outputNode, global);

        insert(syntaxTree, global);

        /*
                Verifica se a função main existe
        */
        if ((hashValue = lookup_element_symtab(global, "main") == -1)) {
                symtab_error(-1, "main", "Função main nao encontrada");
        }

        ElementsSymTab * element;
        element = global->hashTable[hashValue];

        while ((element != NULL) && (strcmp("main", element->name) != 0)){
                element = element->next;
        }

        /*
                Verifica se a função main tem um retorno de inteiro
        */
        if (strcmp(element->type, "Void")) {
                symtab_error(element->lines->line, "main", "Função main deve retornar um inteiro");
        }

        /*
                Caso a funcao main nao eh a ultima funcao do arquivo
        */
        TreeNode * syntaxTreeSibling;
        syntaxTreeSibling = syntaxTree;

        while (syntaxTreeSibling->sibling != NULL) {
                syntaxTreeSibling = syntaxTreeSibling->sibling;
        }

        if (strcmp(syntaxTreeSibling->atribute->name, "main") != 0) {
                symtab_error(-1, "main", "Função main nao eh a ultima função do arquivo");
        }

        printf("\nAnalise de tipos:\n\n");

        /*      
                Verifica se as expressoes que tem funcao como operandos eh inteiro
        */
        type_check_operands(global, syntaxTree);

        printf("        Analise de tipos concluida\n\n");

        printf("\nTabela de simbolos:\n\n");
        print_symtab(global);

        free_node_tree(inputNode);
        free_node_tree(outputNode);
        free_symtab(global);
}
