/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
Criado po Lucas Moura da Silva
e baseado na linguagem TINY escrito por
Kenneth C. Louden
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

#ifndef _ANALYZE_H_
#define _ANALYZE_H_

#include "global.h"
#include "symtab.h"
#include "util.h"

/*
        Construcao da tabela de simbolos junto da checagem de erros
*/
void build_symtab(TreeNode * );

#endif