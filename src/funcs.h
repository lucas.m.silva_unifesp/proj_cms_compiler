/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
Criado po Lucas Moura da Silva
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

#ifndef _FUNCS_H_
#define _FUNCS_H_

#include "global.h"

/*
    funcao de alocacao do buffer
*/
BufferStruct * allocate_buffer(void);

/*
    funcao de alocacao do lexema
*/
LexemaStruct * allocate_lexema (void);

/*
    funcao captura uma string do arquivo
    armazenando no buffer, posteriormente
    sera mandado caracter por caracter do buffer
    ate que nao tenha mais nada no buffer para 
    ser mandado
*/
char get_next_char(BufferStruct * );

/*
    procedimento para voltar um caractere
    do buffer
*/
void unget_char(BufferStruct * );

/*
    procedimento de desalocacao do buffer
*/
void deallocate_buffer(BufferStruct * );

/*
    procedimento de desalocacao do lexema
*/
void deallocate_lexema(LexemaStruct * );

#endif