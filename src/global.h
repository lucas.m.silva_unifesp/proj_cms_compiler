/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
Criado po Lucas Moura da Silva
e baseado na linguagem TINY escrito por
Kenneth C. Louden
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

#ifndef _GLOBAL_H_
#define _GLOBAL_H_

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#ifndef YYPARSER

#include "parse.tab.h"

/*
    ENDFILE eh definido implicitamente por Yacc/Bison
    e nao eh incluido arquivo tab.h
*/
#define ENDFILE 0

#endif

#ifndef TRUE
#define TRUE 1
#endif

#ifndef FALSE
#define FALSE 0
#endif


/*******************************************************************/
/************************* Analise  Lexica *************************/
/*******************************************************************/


// quantidade de caracteres que o buffer suporta
#define BUFFERSIZE 64

// tamanho maximo de um token
#define MAXLEXEMALENTH 64

#define MAXSTATE 12
#define CHARACTERTYPE 21

typedef int TokenType;

// tokens
// typedef enum {
//     ELSE = 300,
//     IF = 301,
//     INT = 302,
//     RETURN = 303,
//     VOID = 304,
//     WHILE = 305,
//     PLUS = 306,
//     MINUS = 307,
//     TIMES = 308,
//     OVER = 309,
//     LT = 310,
//     LTE = 311,
//     GT = 312,
//     GTE = 313,
//     EQ = 314,
//     NEQ = 315,
//     ASSIGN = 316,
//     SEMI = 317,
//     COMMA = 318,
//     LPAREN = 319,
//     RPAREN = 320,
//     LBRACKET = 321,
//     RBRACKET = 322,
//     LBRACE = 323,
//     RBRACE = 324,
//     ID = 325,
//     NUM = 326,
//     ENDFILE = 327,
//     NONE = 328,

//     ERRO = 400
// } TokenType;

// estados
typedef enum {
    START = 0, 
    INOVER = 1,
    INCOMENT = 2,
    INSTAR = 3,
    INGRETTER = 4,
    INLESS = 5,
    INASSIGN = 6,
    INEXC = 7,
    INEQ = 8, 
    INID = 9, 
    INNUM = 10, 
    DONE = 11,
    INERROR = 12
} StateType;

// strutura do buffer
typedef struct bufferBuild
{
    char bufferCarac[BUFFERSIZE]; // armazenamento dos caracteres
    int qtdBuffer; // quantidade de caracteres dentro do buffer
    int posBuffer; // posicao do caracter dentro do buffer
    int posFile; // posicao da linha do arquivo
} BufferStruct;

// strutura do lexama e do token
typedef struct lexemaBuild
{
    char lexema[MAXLEXEMALENTH]; // armazenamento do lexema
    int posLexema; // posicao do lexema
    int lexemaToken; // token do lexema
    int line; // linha do arquivo que aparece o lexema
} LexemaStruct;

// estrutura do elemento da tabela hash
typedef struct element
{
    char* reservedWord;
    TokenType tokenReservedWord;
    struct element * next;
} Element;

// estrutura da tabela hash
typedef struct hashTable
{
    int size;
    Element** table;
} HashTable;



/*******************************************************************/
/*********** Arvore sintatica para a analise sintatica  ************/
/*******************************************************************/


#define MAXCHILDREN 3

/*
    Tipos de nos para a arvore sintatica
*/
typedef enum
{
	StmtK, ExpK 
} NodeKind;

/*
    Tipos de nos para declaracoes
*/
typedef enum
{
	IfK, WhileK, AssignK, VariableK, FunctionK, CallK, ReturnK
} StmtKind;

/*
    Tipos de nos pata expressoes
*/
typedef enum
{
	OpK, ConstK, IdK, ArrayK, IndexVector, TypeK
} ExpKind;

/*
    Tipagem para as declaracoes
*/
typedef enum
{
	Void, Integer
} ExpType;

/*
    Estruturas para a arvore sintatica
*/
typedef union
{ 
    StmtKind stmt; 
    ExpKind exp;
} Kind;

typedef union
{ 
    TokenType operator;
    int value;
    char* name; 
} Atribute;

typedef struct treeNode
{ 
    struct treeNode * child[MAXCHILDREN];
    struct treeNode * sibling;
    int line;
    NodeKind nodeKind;
    ExpType type;
    char* scope;
    int length;

    Kind * kind;
    Atribute * atribute;
} TreeNode;


/*******************************************************************/
/*********** Marcadores para acompanhamento de execucao ************/
/*******************************************************************/


// ponteiro para o arquivo texto
extern FILE * source;

// nome do arquivo
extern char * currentFileName;

// token da ultima leitura
extern TokenType token;

// tabela hash
extern HashTable * hashTable;

// flag de erro
extern int Error;

#endif // _GLOBAL_H_