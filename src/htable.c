/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
Criado po Lucas Moura da Silva
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

#include "htable.h"

static int hash(char * key)
{
        int temp = 0;
        int i = 0;

        while (key[i] != '\0') {
                temp = ((temp << SHIFT) + key[i]) % CAPACITYHASHTABLE;
                ++i;
        }
        return temp;
}

Element * create_element(const char * reservedWord, const TokenType tokenReservedWord)
{
        Element * element = (Element *) malloc(sizeof(Element));
        element->reservedWord = strdup(reservedWord);
        element->tokenReservedWord = tokenReservedWord;
        element->next = NULL;

        return element;
}

HashTable * build_hash_table()
{
        HashTable * hashTable = (HashTable *) malloc(sizeof(HashTable));
        hashTable->size = 0;
        hashTable->table = (Element **) malloc(sizeof(Element *) * CAPACITYHASHTABLE);

        int elementIndex;

        for (elementIndex = 0; elementIndex < CAPACITYHASHTABLE; ++elementIndex) {
                hashTable->table[elementIndex] = NULL;
        }

        return hashTable;
}

int free_element(Element * element)
{
        free(element->reservedWord);
        free(element);
        return 0;
}

int free_hash_table(HashTable * hashTable)
{
        int elementIndex;
        Element * element;
        Element * temp;

        for (elementIndex = 0; elementIndex < CAPACITYHASHTABLE; ++elementIndex) {
                element = hashTable->table[elementIndex];
                while (element != NULL) {
                        temp = element->next;
                        free_element(element);
                        element = temp;
                }
        }
        free(hashTable->table);
        free(hashTable);
        return 0;
}

int insert_element_in_hash_table(HashTable * hashTable, char * reservedWord, TokenType tokenReservedWord)
{
        int elementIndex;
        Element * element;

        elementIndex = hash(reservedWord);

        element = create_element(reservedWord, tokenReservedWord);

        element->next = hashTable->table[elementIndex];
        hashTable->table[elementIndex] = element;

        return 0;
}

int insert_reserved_word_in_hash_table(HashTable * hashTable)
{
        if (!insert_element_in_hash_table(hashTable, "if", IF) &&
        !insert_element_in_hash_table(hashTable, "else", ELSE) &&
        !insert_element_in_hash_table(hashTable, "while", WHILE) &&
        !insert_element_in_hash_table(hashTable, "int", INT) &&
        !insert_element_in_hash_table(hashTable, "void", VOID) &&
        !insert_element_in_hash_table(hashTable, "return", RETURN)) {
                return 0;
        }

        return 1;
}

void print_hash_table(HashTable * hashTable)
{
        int count = 0;
        int elementIndex;
        Element * element;

        for (elementIndex = 0; elementIndex < CAPACITYHASHTABLE; ++elementIndex) {
                element = hashTable->table[elementIndex];

                if (element != NULL) {
                        count++;
                        printf("(%d) ", elementIndex);

                        while (element != NULL) {
                                printf("-> %s; %s ", element->reservedWord, element->tokenReservedWord);
                                element = element->next;
                        }
                        printf("\n");
                }
        }

        printf("%d\n", count);
}

TokenType find_element_in_hash_table(HashTable * hashTable, char * reservedWord, TokenType tokenReservedWord)
{
        int elementIndex;
        Element * element;

        elementIndex = hash(reservedWord);
        element = hashTable->table[elementIndex];

        while (element != NULL) {
                if (!strcmp(element->reservedWord, reservedWord)) {
                        return element->tokenReservedWord;
                }
                element = element->next;
        }

        return tokenReservedWord;
}