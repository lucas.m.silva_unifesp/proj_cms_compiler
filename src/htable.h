/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
Criado po Lucas Moura da Silva
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

#ifndef _HTABLE_H_
#define _HTABLE_H_

#include "global.h"

#define CAPACITYHASHTABLE 11

/*
    SHIFT eh a potencia de dois usada como multiplicador
    na funcao de hashing 
*/
#define SHIFT 5

/*
    Função que cria um novo elemento
*/
Element * create_element(const char * , const TokenType );

/*
    Função que cria uma nova tabela hash
*/
HashTable * build_hash_table(void);

/*
    Função que libera a memoria de um elemento
*/
int free_element(Element * );

/*
    Função que libera a memoria de uma tabela hash
*/
int free_hash_table(HashTable * );

/*
    Função que insere um elemento na tabela hash
*/
int insert_element_in_hash_table(HashTable * , char * , TokenType );

/*
    Função que insere uma palavra reservada na tabela hash
*/
int insert_reserved_word_in_hash_table(HashTable * );

/*
    Função que imprime a tabela hash
*/
void print_hash_table(HashTable * );

/*
    Função que procura um elemento na tabela hash e retorna o token correspondente
*/
TokenType find_element_in_hash_table(HashTable * , char * , TokenType );

#endif