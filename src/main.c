/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
Criado po Lucas Moura da Silva
e baseado na linguagem TINY escrito por
Kenneth C. Louden
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

#include "global.h"
#include "funcs.h"
#include "scan.h"
#include "htable.h"
#include "parse.h"
#include "analyze.h"

int Error = FALSE;

FILE * source;

char * currentFileName;

TokenType token;

HashTable * hashTable;

int main(int argc, char * argv[])
{
    if (argc != 2) {
        printf("Erro ao ler os parametros\n");
        exit(1);
    }

    currentFileName = argv[1];

    source = fopen(argv[1], "r");

    if (source == NULL) {
        printf("Erro ao alocar memoria para o arquivo texto\n");
        exit(1);
    }

    BufferStruct * buffer = allocate_buffer();
    LexemaStruct * lexema = allocate_lexema();

    if (buffer == NULL || lexema == NULL) {
        printf("Memoria insuficiente para alocacao do buffer\n");
        exit(1);
    }

    hashTable = build_hash_table();
    if (insert_reserved_word_in_hash_table(hashTable)) {
        printf("Erro ao inserir as palavras reservadas na tabela hash\n");
        exit(1);
    }

    printf("\n-=-=-=-=--=-=--=-=-=-=-=-=-=-=-=-=-=-\n");
    printf("\n            Analise lexica:          \n");
    printf("\n-=-=-=-=--=-=--=-=-=-=-=-=-=-=-=-=-=-\n\n");

    printf("\nLinha:     Lexema:           Token:\n\n");

    TreeNode * syntaxTree;

    syntaxTree = parse(buffer, lexema);

    if (Error) {
        deallocate_buffer(buffer);
        deallocate_lexema(lexema);

        exit(1);
    }

    printf("\n-=-=-=-=--=-=--=-=-=-=-=-=-=-=-=-=-=-\n");
    printf("\n          Analise sintatica:         \n");
    printf("\n-=-=-=-=--=-=--=-=-=-=-=-=-=-=-=-=-=-\n\n");

    printf("\n Arvore sintatica: \n\n");

    print_tree(syntaxTree);

    printf("\n-=-=-=-=--=-=--=-=-=-=-=-=-=-=-=-=-=-\n");
    printf("\n          Analise semantica:         \n");
    printf("\n-=-=-=-=--=-=--=-=-=-=-=-=-=-=-=-=-=-\n\n");

    build_symtab(syntaxTree);

    free_node_tree(syntaxTree);

    deallocate_buffer(buffer);
    deallocate_lexema(lexema);
    
    free_hash_table(hashTable);
    
    fclose(source);

    return 0;
}