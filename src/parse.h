/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
Criado po Lucas Moura da Silva
e baseado na linguagem TINY escrito por
Kenneth C. Louden
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

#ifndef _PARSE_H_
#define _PARSE_H_

#include "global.h"
#include "util.h"
#include "scan.h"

/*
        Função que faz a construção da arvore sintatica
*/
TreeNode * parse(BufferStruct *, LexemaStruct *);

#endif