/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
Criado po Lucas Moura da Silva
e baseado na linguagem TINY escrito por
Kenneth C. Louden
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

%{
#define YYPARSER
#include "parse.h"

#define YYSTYPE TreeNode *
static YYSTYPE savedTree;
static char * savedNumber;
static int lineBuffer;
static int yylex(void);
static int yyerror(char *);

static BufferStruct * bufferLocal;
static LexemaStruct * lexemaLocal;
%}

%token IF ELSE INT VOID RETURN WHILE
%token ID NUM 
%token PLUS MINUS TIMES OVER LT LTE GT GTE EQ NEQ ASSIGN SEMI COMMA LPAREN RPAREN LBRACKET RBRACKET LBRACE RBRACE
%token NONE // caso os caracteres serem o conjunto comands
%token ERRO ENDFILE

%% /* Grammar for C- */
programa                : declaracao_lista
                          {
                            savedTree = $1;
                          }
                        ;
declaracao_lista        : declaracao_lista declaracao
                          {
                            YYSTYPE node = $1;
                            if (node != NULL) {
                              while (node->sibling != NULL)
                                node = node->sibling;
                              node->sibling = $2;
                            } 
                            else 
                              $$ = $2;
                          }
                        | declaracao
                        ;
declaracao              : var_declaracao
                        | fun_declaraco
                        ;
var_declaracao          : tipo_especificador identifier SEMI
                          {
                            $$ = $2;
                            $2->nodeKind = StmtK;
                            $2->kind->stmt = VariableK;
                            $2->child[0] = $1;

                            if (strcmp($1->atribute->name, "integer")) {
                              $2->type = Void;
                            } else {
                              $2->type = Integer;
                            }
                          }
                        | tipo_especificador identifier LBRACKET number RBRACKET SEMI
                          {
                            $$ = $2;
                            $2->nodeKind = StmtK;
                            $2->kind->stmt = VariableK;
                            $4->kind->exp = IndexVector;
                            $2->length = $4->atribute->value;
                            $2->child[0] = $1;
                            $2->child[1] = $4;

                            if (strcmp($1->atribute->name, "integer")) {
                              $2->type = Void;
                            } else {
                              $2->type = Integer;
                            }
                          }
                        ;
tipo_especificador      : INT
                          {
                            $$ = new_exp_node(TypeK, lineBuffer);
                            $$->atribute->name = "integer";
                          }
                        | VOID
                          {
                            $$ = new_exp_node(TypeK, lineBuffer);
                            $$->atribute->name  = "void";
                          }
                        ;
fun_declaraco           : tipo_especificador identifier LPAREN param_lista RPAREN composto_decl
                          {
                            $$ = $2;
                            $2->nodeKind = StmtK;
                            $2->kind->stmt = FunctionK;
                            $$->child[0] = $1;
                            $$->child[1] = $4;
                            $$->child[2] = $6;

                            if (strcmp($1->atribute->name, "integer")) {
                              $2->type = Void;
                            } else {
                              $2->type = Integer;
                            }
                          }
                        ;
param_lista             : param_lista COMMA param
                          {
                            YYSTYPE node = $1;
                            if (node != NULL) {
                              while (node->sibling != NULL)
                                node = node->sibling;
                              node->sibling = $3;
                            } 
                            else 
                              $$ = $3;
                          }
                        | param
                        | VOID
                          {
                            $$ = new_exp_node(TypeK, lineBuffer);
                            $$->atribute->name  = "void";
                          }
                        ;
param                   : tipo_especificador identifier 
                          {
                            $$ = $2;
                            $2->nodeKind = StmtK;
                            $2->kind->stmt = VariableK;
                            $$->child[0] = $1;

                            if (strcmp($1->atribute->name, "integer")) {
                              $2->type = Void;
                            } else {
                              $2->type = Integer;
                            }
                          }
                        | tipo_especificador identifier LBRACKET RBRACKET
                          {
                            $$ = $2;
                            $2->nodeKind = StmtK;
                            $2->kind->stmt = VariableK;
                            $$->child[0] = $1;

                            if (strcmp($1->atribute->name, "integer")) {
                              $2->type = Void;
                            } else {
                              $2->type = Integer;
                            }
                          }
                        ;
composto_decl           : LBRACE local_declaracoes statement_lista RBRACE
                          {
                            YYSTYPE node = $2;
                            if (node != NULL) {
                              while (node->sibling != NULL)
                                node = node->sibling;
                              node->sibling = $3;
                              $$ = $2;
                            } 
                            else 
                              $$ = $3;
                          }
                        ;
local_declaracoes       : local_declaracoes var_declaracao
                          {
                            YYSTYPE node = $1;
                            if (node != NULL) {
                              while (node->sibling != NULL)
                                node = node->sibling;
                              node->sibling = $2;
                            } 
                            else 
                              $$ = $2;
                          }
                        | /* vazio */
                          {
                            $$ = NULL;
                          }
                        ;
statement_lista         : statement_lista statement
                          {
                            YYSTYPE node = $1;
                            if (node != NULL) {
                              while (node->sibling != NULL)
                                node = node->sibling;
                              node->sibling = $2;
                            } 
                            else 
                              $$ = $2;
                          }
                        | /* vazio */               
                          {
                            $$ = NULL;
                          }        
                        ;
statement               : expressao_decl
                        | composto_decl
                        | selecao_decl
                        | iteracao_decl
                        | retorno_decl
                        ;          
expressao_decl          : expressao SEMI
                        | SEMI
                          {
                            $$ = NULL;
                          }
                        ;
selecao_decl            : IF LPAREN expressao RPAREN statement
                          {
                            $$ = new_stmt_node(IfK, lineBuffer);
                            $$->child[0] = $3;
                            $$->child[1] = $5;
                          }
                        | IF LPAREN expressao RPAREN statement ELSE statement
                          {
                            $$ = new_stmt_node(IfK, lineBuffer);
                            $$->child[0] = $3;
                            $$->child[1] = $5;
                            $$->child[2] = $7;
                          }
                        ;
iteracao_decl           : WHILE LPAREN expressao RPAREN statement
                          {
                            $$ = new_stmt_node(WhileK, lineBuffer);
                            $$->child[0] = $3;
                            $$->child[1] = $5;
                          }
                        ;
retorno_decl            : RETURN SEMI
                          {
                            $$ = new_stmt_node(ReturnK, lineBuffer);
                          }
                        | RETURN expressao SEMI
                          {
                            $$ = new_stmt_node(ReturnK, lineBuffer);
                            $$->child[0] = $2;
                          }
                        ;
expressao               : var ASSIGN expressao
                          {
                            $$ = new_stmt_node(AssignK, lineBuffer);
                            $$->atribute->operator = ASSIGN;
                            $$->child[0] = $1;
                            $$->child[1] = $3;
                          }
                        | simples_expressao
                        ;
var                     : identifier
                          {
                            $$->nodeKind = ExpK;
                          }
                        | identifier LBRACKET expressao RBRACKET
                          {
                            $$->nodeKind = ExpK;
                            $$->child[0] = $3;
                            $$->kind->exp = ArrayK;
                          }
                        ;
simples_expressao       : soma_expressao relacional soma_expressao
                          {
                            $$ = $2;
                            $$->child[0] = $1;
                            $$->child[1] = $3;
                          }
                        | soma_expressao
                        ;
relacional              : LTE
                          {
                            $$ = new_exp_node(OpK, lineBuffer);
                            $$->atribute->operator = LTE;
                          }
                        | LT
                          {
                            $$ = new_exp_node(OpK, lineBuffer);
                            $$->atribute->operator = LT;
                          }
                        | GT
                          {
                            $$ = new_exp_node(OpK, lineBuffer);
                            $$->atribute->operator = GT;
                          }
                        | GTE
                          {
                            $$ = new_exp_node(OpK, lineBuffer);
                            $$->atribute->operator = GTE;
                          }
                        | EQ
                          {
                            $$ = new_exp_node(OpK, lineBuffer);
                            $$->atribute->operator = EQ;
                          }
                        | NEQ
                          {
                            $$ = new_exp_node(OpK, lineBuffer);
                            $$->atribute->operator = NEQ;
                          }
                        ;
soma_expressao          : soma_expressao soma termo
                          {
                            $$ = $2;
                            $$->child[0] = $1;
                            $$->child[1] = $3;
                          }
                        | termo
                        ;
soma                    : PLUS
                          {
                            $$ = new_exp_node(OpK, lineBuffer);
                            $$->atribute->operator = PLUS;
                          }
                        | MINUS
                          {
                            $$ = new_exp_node(OpK, lineBuffer);
                            $$->atribute->operator = MINUS;
                          }
                        ;
termo                   : termo mult fator
                          {
                            $$ = $2;
                            $$->child[0] = $1;
                            $$->child[1] = $3;
                          }
                        | fator
                        ;
mult                    : TIMES
                          {
                            $$ = new_exp_node(OpK, lineBuffer);
                            $$->atribute->operator = TIMES;
                          }
                        | OVER
                          {
                            $$ = new_exp_node(OpK, lineBuffer);
                            $$->atribute->operator = OVER;
                          }
                        ;
fator                   : LPAREN expressao RPAREN
                          {
                            $$ = $2;
                          }
                        | var
                        | ativacao
                        | number
                        ;
ativacao                : identifier LPAREN args RPAREN
                          {
                            $$->child[0] = $3;
                            $$->nodeKind = StmtK;
                            $$->kind->stmt = CallK;
                          }
                        ;
args                    : arg_lista
                        | /* vazio */
                          {
                            $$ = NULL;
                          }
                        ;
arg_lista               : arg_lista COMMA expressao
                          {
                            YYSTYPE node = $1;
                            if (node != NULL) {
                              while (node->sibling != NULL)
                                node = node->sibling;
                              node->sibling = $3;
                            } 
                            else 
                              $$ = $3;
                          }
                        | expressao
                        ;
identifier              : ID
                          {
                            $$ = new_exp_node(IdK, lineBuffer);
                            $$->atribute->name = copy_string(lexemaLocal->lexema, lineBuffer);
                          }
                        ;                     
number                  : NUM
                          {
                            $$ = new_exp_node(ConstK, lineBuffer);
                            savedNumber = copy_string(lexemaLocal->lexema, lineBuffer);
                            $$->atribute->value = atoi(savedNumber);
                            free(savedNumber);
                          }


%%

int yyerror(char * message)
{ 
  printf("\nERRO SINTÁTICO: ");
  printf("LINHA: %i ", lineBuffer);
  printf("TOKEN:");
  print_token_type(yychar);
  Error = TRUE;
  return 0;
}

static int yylex(void)
{
  get_token(bufferLocal, lexemaLocal);
  while (lexemaLocal->lexemaToken == NONE) {
    get_token(bufferLocal, lexemaLocal);
  }

  lineBuffer = lexemaLocal->line;

  return lexemaLocal->lexemaToken;
}

TreeNode * parse(BufferStruct * buffer, LexemaStruct * lexema)
{
  bufferLocal = buffer;
  lexemaLocal = lexema;
  yyparse();
  return savedTree;
}
