/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
Criado po Lucas Moura da Silva
e baseado no algoritmo do analisador lexico dirigido por tabela 
apresentado no livro Compiladores principios e praticas
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

#include "scan.h"

StateType transitionTable[MAXSTATE-1][CHARACTERTYPE] = {
//              {bar,       star,       plus,       minus,      semi,       comma,      lparen,     rparen,     lbracket,   rbracket,   lbraces,    rbraces,    greater,    less,       exclamation,    equals,     letter,     digit,      comands,    eof,        other}
/*START*/       {INOVER,    DONE,       DONE,       DONE,       DONE,       DONE,       DONE,       DONE,       DONE,       DONE,       DONE,       DONE,       INGRETTER,  INLESS,     INEXC,          INASSIGN,   INID,       INNUM,      DONE,       DONE,       INERROR},
/*INOVER*/      {DONE,      INCOMENT,   DONE,       DONE,       DONE,       DONE,       DONE,       DONE,       DONE,       DONE,       DONE,       DONE,       DONE,       DONE,       DONE,           DONE,       DONE,       DONE,       DONE,       DONE,       DONE},
/*INCOMENT*/    {INCOMENT,  INSTAR,     INCOMENT,   INCOMENT,   INCOMENT,   INCOMENT,   INCOMENT,   INCOMENT,   INCOMENT,   INCOMENT,   INCOMENT,   INCOMENT,   INCOMENT,   INCOMENT,   INCOMENT,       INCOMENT,   INCOMENT,   INCOMENT,   INCOMENT,   DONE,       INCOMENT},
/*INSTAR*/      {START,     INCOMENT,   INCOMENT,   INCOMENT,   INCOMENT,   INCOMENT,   INCOMENT,   INCOMENT,   INCOMENT,   INCOMENT,   INCOMENT,   INCOMENT,   INCOMENT,   INCOMENT,   INCOMENT,       INCOMENT,   INCOMENT,   INCOMENT,   INCOMENT,   DONE,       INCOMENT},
/*INGRETTER*/   {DONE,      DONE,       DONE,       DONE,       DONE,       DONE,       DONE,       DONE,       DONE,       DONE,       DONE,       DONE,       DONE,       DONE,       DONE,           INEQ,       DONE,       DONE,       DONE,       DONE,       DONE},
/*INLESS*/      {DONE,      DONE,       DONE,       DONE,       DONE,       DONE,       DONE,       DONE,       DONE,       DONE,       DONE,       DONE,       DONE,       DONE,       DONE,           INEQ,       DONE,       DONE,       DONE,       DONE,       DONE},
/*INASSIGN*/    {DONE,      DONE,       DONE,       DONE,       DONE,       DONE,       DONE,       DONE,       DONE,       DONE,       DONE,       DONE,       DONE,       DONE,       DONE,           INEQ,       DONE,       DONE,       DONE,       DONE,       DONE},
/*INEXC*/       {INERROR,   INERROR,    INERROR,    INERROR,    INERROR,    INERROR,    INERROR,    INERROR,    INERROR,    INERROR,    INERROR,    INERROR,    INERROR,    INERROR,    INERROR,        INEQ,       INERROR,    INERROR,    INERROR,    INERROR,    INERROR}, 
/*INEQ*/        {DONE,      DONE,       DONE,       DONE,       DONE,       DONE,       DONE,       DONE,       DONE,       DONE,       DONE,       DONE,       DONE,       DONE,       DONE,           DONE,       DONE,       DONE,       DONE,       DONE,       DONE},
/*INID*/        {DONE,      DONE,       DONE,       DONE,       DONE,       DONE,       DONE,       DONE,       DONE,       DONE,       DONE,       DONE,       DONE,       DONE,       DONE,           DONE,       INID,       DONE,       DONE,       DONE,       DONE},
/*INNUM*/       {DONE,      DONE,       DONE,       DONE,       DONE,       DONE,       DONE,       DONE,       DONE,       DONE,       DONE,       DONE,       DONE,       DONE,       DONE,           DONE,       DONE,       INNUM,      DONE,       DONE,       DONE}
};

int advenceTable[MAXSTATE-1][CHARACTERTYPE] = { 
//              {bar,   star,   plus,   minus,  semi,   comma,  lparen, rparen, lbracket,   rbracket,   lbraces,    rbraces,    greater,    less,   exclamation,    equals, letter, digit,  comands,    eof,    other}
/*START*/       {TRUE,  FALSE,  FALSE,  FALSE,  FALSE,  FALSE,  FALSE,  FALSE,  FALSE,      FALSE,      FALSE,      FALSE,      TRUE,       TRUE,   TRUE,           TRUE,   TRUE,   TRUE,   FALSE,      FALSE,  FALSE},
/*INOVER*/      {FALSE, TRUE,   FALSE,  FALSE,  FALSE,  FALSE,  FALSE,  FALSE,  FALSE,      FALSE,      FALSE,      FALSE,      FALSE,      FALSE,  FALSE,          FALSE,  FALSE,  FALSE,  FALSE,      FALSE,  FALSE},
/*INCOMENT*/    {TRUE,  TRUE,   TRUE,   TRUE,   TRUE,   TRUE,   TRUE,   TRUE,   TRUE,       TRUE,       TRUE,       TRUE,       TRUE,       TRUE,   TRUE,           TRUE,   TRUE,   TRUE,   TRUE,       TRUE,   TRUE},
/*INSTAR*/      {TRUE,  TRUE,   TRUE,   TRUE,   TRUE,   TRUE,   TRUE,   TRUE,   TRUE,       TRUE,       TRUE,       TRUE,       TRUE,       TRUE,   TRUE,           TRUE,   TRUE,   TRUE,   TRUE,       FALSE,  TRUE},
/*INGRETTER*/   {FALSE, FALSE,  FALSE,  FALSE,  FALSE,  FALSE,  FALSE,  FALSE,  FALSE,      FALSE,      FALSE,      FALSE,      FALSE,      FALSE,  FALSE,          TRUE,   FALSE,  FALSE,  FALSE,      FALSE,  FALSE},
/*INLESS*/      {FALSE, FALSE,  FALSE,  FALSE,  FALSE,  FALSE,  FALSE,  FALSE,  FALSE,      FALSE,      FALSE,      FALSE,      FALSE,      FALSE,  FALSE,          TRUE,   FALSE,  FALSE,  FALSE,      FALSE,  FALSE},
/*INASSIGN*/    {FALSE, FALSE,  FALSE,  FALSE,  FALSE,  FALSE,  FALSE,  FALSE,  FALSE,      FALSE,      FALSE,      FALSE,      FALSE,      FALSE,  FALSE,          TRUE,   FALSE,  FALSE,  FALSE,      FALSE,  FALSE},
/*INEXC*/       {FALSE, FALSE,  FALSE,  FALSE,  FALSE,  FALSE,  FALSE,  FALSE,  FALSE,      FALSE,      FALSE,      FALSE,      FALSE,      FALSE,  FALSE,          TRUE,   FALSE,  FALSE,  FALSE,      FALSE,  FALSE},
/*INEQ*/        {FALSE, FALSE,  FALSE,  FALSE,  FALSE,  FALSE,  FALSE,  FALSE,  FALSE,      FALSE,      FALSE,      FALSE,      FALSE,      FALSE,  FALSE,          FALSE,  FALSE,  FALSE,  FALSE,      FALSE,  FALSE},
/*INID*/        {FALSE, FALSE,  FALSE,  FALSE,  FALSE,  FALSE,  FALSE,  FALSE,  FALSE,      FALSE,      FALSE,      FALSE,      FALSE,      FALSE,  FALSE,          FALSE,  TRUE,   FALSE,  FALSE,      FALSE,  FALSE},
/*INNUM*/       {FALSE, FALSE,  FALSE,  FALSE,  FALSE,  FALSE,  FALSE,  FALSE,  FALSE,      FALSE,      FALSE,      FALSE,      FALSE,      FALSE,  FALSE,          FALSE,  FALSE,  TRUE,   FALSE,      FALSE,  FALSE}
};

int acceptationTable[MAXSTATE+1] = {
//  {   START, INOVER,  INCOMENT,   INSTAR, INGRETTER,  INLESS, INASSIGN,   INEXC, INEQ,    INID,   INNUM, DONE, INERROR}
        FALSE, FALSE,   FALSE,      FALSE,  FALSE,      FALSE,  FALSE,      FALSE, FALSE,   FALSE,  FALSE, TRUE, FALSE
};

TokenType tokenTable[1][CHARACTERTYPE] = {
//              {bar,   star,   plus,   minus,  semi,   comma,  lparen, rparen, lbracket,   rbracket,   lbraces,    rbraces,    greater,    less,   exclamation,    equals, letter, digit,  comands,    eof,        other}
/*START*/       {OVER,  TIMES,  PLUS,   MINUS,  SEMI,   COMMA,  LPAREN, RPAREN, LBRACKET,   RBRACKET,   LBRACE,     RBRACE,     GT,         LT,     NEQ,            ASSIGN, ID,     NUM,    NONE,       ENDFILE,    ERRO},
};

static int is_alpha(char caracter)
{
    return (caracter >= 'a' && caracter <= 'z') || (caracter >= 'A' && caracter <= 'Z');
}

static int is_digit(char caracter)
{
    return (caracter >= '0' && caracter <= '9');
}

static int is_comands(char caracter)
{
    return (caracter >= 0 && caracter <= 32);
}

static int is_bar(char character)
{
    return (character == '/');
}

static int is_star(char character)
{
    return (character == '*');
}

static int is_plus(char caracter)
{
    return (caracter == '+');
}

static int is_minus(char character)
{
    return (character == '-');
}

static int is_semi(char character)
{
    return (character == ';');
}

static int is_comma(char character)
{
    return (character == ',');
}

static int is_lparenthesis(char character)
{
    return (character == '(');
}

static int is_rparenthesis(char character)
{
    return (character == ')');
}

static int is_lbracket(char character)
{
    return (character == '[');
}

static int is_rbracket(char character)
{
    return (character == ']');
}

static int is_lbraces(char character)
{
    return (character == '{');
}

static int is_rbraces(char character)
{
    return (character == '}');
}

static int is_greater(char character)
{
    return (character == '>');
}

static int is_less(char character)
{
    return (character == '<');
}

static int is_exclamation(char character)
{
    return (character == '!');
}

static int is_equals(char caracter)
{
    return (caracter == '=');
}

static int get_character_type(char caracter)
{
    if (is_bar(caracter)) {
        return 0;
    } else if (is_star(caracter)) {
        return 1;
    } else if (is_plus(caracter)) {
        return 2;
    } else if (is_minus(caracter)) {
        return 3;
    } else if (is_semi(caracter)) {
        return 4;
    } else if (is_comma(caracter)) {
        return 5;
    } else if (is_lparenthesis(caracter)) {
        return 6;
    } else if (is_rparenthesis(caracter)) {
        return 7;
    } else if (is_lbracket(caracter)) {
        return 8;
    } else if (is_rbracket(caracter)) {
        return 9;
    } else if (is_lbraces(caracter)) {
        return 10;
    } else if (is_rbraces(caracter)) {
        return 11;
    } else if (is_greater(caracter)) {
        return 12;
    } else if (is_less(caracter)) {
        return 13;
    } else if (is_exclamation(caracter)) {
        return 14;
    } else if (is_equals(caracter)) {
        return 15;
    } else if (is_alpha(caracter)) {
        return 16;
    } else if (is_digit(caracter)) {
        return 17;
    } else if (is_comands(caracter)) {
        return 18;
    } else if (caracter == EOF) {
        return 19;
    } else {
        return 20;
    }
}

void get_token(BufferStruct * buffer, LexemaStruct * lexema)
{
        
    StateType state = START;
    StateType newState;
    char character = get_next_char(buffer);
    int characterType;
    int canPrint = TRUE;

    strncpy(lexema->lexema, " ", 1);
    lexema->posLexema = -1;

    while ((!acceptationTable[state]) && (state != INERROR)) {
        characterType = get_character_type(character);
        newState = transitionTable[state][characterType];

        if ((state != INCOMENT) && (state != INSTAR)) {
            lexema->posLexema++;
            lexema->lexema[lexema->posLexema] = character;

            switch (state) {
                case START:
                    lexema->line = buffer->posFile;
                    if (characterType == 18) {
                        canPrint = FALSE;
                    } else {
                        lexema->lexemaToken = tokenTable[state][characterType];
                    }
                    break;
                case INOVER:
                    if (characterType == 1) {
                        strncpy(lexema->lexema, " ", 1);
                        lexema->posLexema = -1;
                    } else {
                        lexema->posLexema--;
                        unget_char(buffer);
                    }
                    break;
                case INGRETTER:
                    if (characterType != 15) {
                        lexema->posLexema--;
                        unget_char(buffer);
                    } else {
                        lexema->lexemaToken = GTE;
                    }
                    break;
                case INLESS:
                    if (characterType != 15) {
                        lexema->posLexema--;
                        unget_char(buffer);
                    } else {
                        lexema->lexemaToken = LTE;
                    }
                    break;
                case INASSIGN:
                    if (characterType != 15) {
                        lexema->posLexema--;
                        unget_char(buffer);
                    } else {
                        lexema->lexemaToken = EQ;
                    }
                    break;
                case INEXC:
                    if (characterType != 15) {
                        lexema->posLexema--;
                        unget_char(buffer);
                    } 
                    break;
                case INEQ:
                    lexema->posLexema--;
                    unget_char(buffer);
                    break;
                case INID:
                    if (characterType != 16) {
                        lexema->posLexema--;
                        unget_char(buffer);
                    }
                    break;
                case INNUM:
                    if (characterType != 17) {
                        lexema->posLexema--;
                        unget_char(buffer);
                    } 
                    break;
                default:
                    break;
            }
        } else {
            if (characterType == 19) {
                lexema->lexemaToken = ENDFILE;
                lexema->line = buffer->posFile;
                canPrint = FALSE;
                printf("\n%s: ERRO SINTATICO: LINHA: %d comentario nao terminado\n", currentFileName, lexema->line);
                exit(1);
            }
        }

        if (advenceTable[state][characterType]) {
            character = get_next_char(buffer);
        }
        
        state = newState;
    }

    lexema->posLexema++;
    lexema->lexema[lexema->posLexema] = '\0';

    if ((!acceptationTable[state]) && (state == INERROR)) {
        lexema->lexemaToken = ERRO;
        Error = TRUE;
    } 

    if (lexema->lexemaToken == ID) {
        lexema->lexemaToken = find_element_in_hash_table(hashTable, lexema->lexema, lexema->lexemaToken);
    }

    if (canPrint) {
        print_token(lexema->lexemaToken, lexema->lexema, lexema->line);
    } else {
        lexema->lexemaToken = NONE;
    }
}