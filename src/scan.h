/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
Criado po Lucas Moura da Silva
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

#ifndef _SCAN_H_
#define _SCAN_H_

#include "global.h"
#include "funcs.h"
#include "util.h"
#include "htable.h"

/*
        Funcao que retorna o token correspondente ao lexema
*/
void get_token(BufferStruct * , LexemaStruct * );

#endif