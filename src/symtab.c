/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
Criado po Lucas Moura da Silva
e baseado na linguagem TINY escrito por
Kenneth C. Louden
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

#include "symtab.h"

static int hash_symtab(char * key)
{
        int temp = 0;
        int i = 0;

        while (key[i] != '\0') {
                temp = ((temp << SHIFTSYMTAB + key[i]) % SYMTABSIZE);
                ++i;
        }
        return temp;
}

ScopeList * build_scope_list(char * scopeName, ScopeList * parent)
{
        ScopeList * scope;

        scope = (ScopeList *) malloc(sizeof(struct scopeList));
        scope->scope = scopeName;
        scope->parent = parent;
        scope->child = NULL;
        scope->sibling = NULL;

        return scope;
}

void insert_element_symtab(ScopeList * scope, char * name, int line, char * type, char * nodeType, int hashValue)
{
        int hash;

        // int hash = hashValue == -1? hash_symtab(name) : hashValue;        
        if (hashValue == -1) {
                hash = hash_symtab(name);
        } else {
                hash = hashValue;
        }

        ElementsSymTab * element;
        element = scope->hashTable[hash];

        while ((element != NULL) && (strcmp(name, element->name) != 0)){
                element = element->next;
        }

        if (element == NULL) { // variavel ainda nao na tabela
                element = (ElementsSymTab *) malloc(sizeof(struct elementsSymTab));

                element->name = name;

                element->lines = (LineList *) malloc(sizeof(struct lineList));

                element->lines->line = line;
                element->lines->next = NULL;
                element->type = type;
                element->nodeType = nodeType;
                element->next = scope->hashTable[hash];
                scope->hashTable[hash] = element;

        } else { // encontrada na tabela, entao acrescente numero de linha
                LineList * elementLines;
                elementLines = element->lines;

                while (elementLines->next != NULL){
                        elementLines = elementLines->next;
                }

                elementLines->next = (LineList *) malloc(sizeof(struct lineList));

                elementLines->next->line = line;
                elementLines->next->next = NULL;
        }
}

int lookup_element_symtab_nodeType(ScopeList * scope, char * name, char * nodeType)
{
        int hash = hash_symtab(name);

        ElementsSymTab * element = scope->hashTable[hash];

        while ((element != NULL) && (strcmp(name, element->name) != 0)) {
                element = element->next;
        }

        if (element == NULL) {
                return -1;
        } else {
                return strcmp(nodeType, element->nodeType);
        }
}


int lookup_element_symtab(ScopeList * scope, char * name)
{
        int hash = hash_symtab(name);

        ElementsSymTab * element = scope->hashTable[hash];

        while ((element != NULL) && (strcmp(name, element->name) != 0)) {
                element = element->next;
        }

        if (element == NULL) {
                return -1;
        } else {
                return hash;       
        }
}

void print_symtab(ScopeList * scopeRoot) {
        ScopeList * scope;
        scope = scopeRoot;

        int hashValue;

        printf("Escopo    Nome de Variavel   Tipo      Tipo do No    Numeros das linhas\n");
        printf("- - -     - - - - - - - -    - -       - - - - -     - - - - - - - - - \n");

        while (scope != NULL) {
                for (hashValue=0; hashValue<SYMTABSIZE; ++hashValue) {
                        if (scope->hashTable[hashValue] != NULL) {
                                ElementsSymTab * element = scope->hashTable[hashValue];

                                while (element != NULL) {
                                        LineList * elementLines = element->lines;
                                        
                                        printf("%-10s", scope->scope);
                                        printf("%-19s", element->name);
                                        printf("%-10s", element->type);
                                        printf("%-13s", element->nodeType);

                                        while (elementLines != NULL) {
                                                printf("%-4d", elementLines->line);
                                                elementLines = elementLines->next;
                                        }

                                        printf("\n");

                                        element = element->next;
                                }
                        }
                }

                if (scope->child != NULL) {
                        scope = scope->child;
                } else if (scope->sibling != NULL) {
                        scope = scope->sibling;
                } else { 
                        scope = NULL;
                }
        }
}

int free_lines_symtab(LineList * lines)
{
        if (lines != NULL) {
                free_lines_symtab(lines->next);
                free(lines);
        }
}

int free_element_symtab(ElementsSymTab * element)
{
        if (element != NULL) {
                free_lines_symtab(element->lines);
        }

        return 0;
}

int free_symtab(ScopeList * symtab)
{
        if (symtab != NULL) {
                free_symtab(symtab->child);
                free_symtab(symtab->sibling);

                int hashValue;
                ElementsSymTab * element;
                ElementsSymTab * temp;

                for (hashValue = 0; hashValue < SYMTABSIZE; ++hashValue) {
                        element = symtab->hashTable[hashValue];
                        while (element != NULL) {
                                temp = element->next;
                                free_element_symtab(element);
                                free(element);
                                element = temp;
                        }
                }

                free(symtab);
        }
        
        return 0;
}