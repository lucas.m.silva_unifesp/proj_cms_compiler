/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
Criado po Lucas Moura da Silva
e baseado na linguagem TINY escrito por
Kenneth C. Louden
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

#ifndef _SYMTAB_H_
#define _SYMTAB_H_

#include "global.h"

#define SYMTABSIZE 101
#define SHIFTSYMTAB 4

/*
    Estrutura para a tabela de simbolo (tabela hash)
*/
typedef struct lineList
{
    int line;
    struct lineList * next;
} LineList;


typedef struct elementsSymTab
{
    char * name;
    LineList * lines;
    char * type;
    char * nodeType;
    struct elementsSymTab * next;
} ElementsSymTab;


typedef struct scopeList
{
    char * scope;
    ElementsSymTab * hashTable[SYMTABSIZE];
    struct scopeList * parent;
    struct scopeList * child;
    struct scopeList * sibling;
} ScopeList;

/*
    Função que cria uma nova tabela de simbolos para o escopo
*/
ScopeList * build_scope_list(char * scopeName, ScopeList * );

/*
    Procedimento que insere um elemento na tabela de simbolos
*/
void insert_element_symtab(ScopeList * , char * name, int line, char * type, char * nodeType, int hashValue);

/*
    Procedimento que remove um elemento da tabela de simbolos
*/
void remove_element_symtab(ScopeList * , char * name);

/*
    Função que procura o tipo de um elemento na tabela de simbolos
*/
int lookup_element_symtab_nodeType(ScopeList * , char * name, char * nodeType);

/*
    Função que procura um elemento na tabela de simbolos
*/
int lookup_element_symtab(ScopeList * , char * name);

/*
    Procedimento que imprime a tabela de simbolos
*/
void print_symtab(ScopeList * );

/*
    Função que libera a memoria das linhas de um elemento da tabela de simbolos
*/
int free_lines_symtab(LineList * );

/*
    Função que libera a memoria de um elemento da tabela de simbolos
*/
int free_element_symtab(ElementsSymTab * );

/*
    Função que libera a memoria da tabela de simbolos
*/
int free_symtab(ScopeList * );

#endif