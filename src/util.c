#include "util.h"

void print_token_type(TokenType tokenType)
{
        switch (tokenType) {
                case ELSE:
                        printf(" ELSE\n");
                        break;
                case IF:
                        printf(" IF\n");
                        break;
                case INT:
                        printf(" INT\n");
                        break;
                case RETURN:
                        printf(" RETURN\n");
                        break;
                case VOID:
                        printf(" VOID\n");
                        break;
                case WHILE:
                        printf(" WHILE\n");
                        break;
                case PLUS:
                        printf(" PLUS\n");
                        break;
                case MINUS:
                        printf(" MINUS\n");
                        break;
                case TIMES:
                        printf(" TIMES\n");
                        break;
                case OVER:
                        printf(" OVER\n");
                        break;
                case LT:
                        printf(" LT\n");
                        break;
                case LTE:
                        printf(" LTE\n");
                        break;
                case GT:
                        printf(" GT\n");
                        break;
                case GTE:
                        printf(" GTE\n");
                        break;
                case EQ:
                        printf(" EQ\n");
                        break;
                case NEQ:
                        printf(" NEQ\n");
                        break;
                case ASSIGN:
                        printf(" ASSIGN\n");
                        break;
                case SEMI:
                        printf(" SEMI\n");
                        break;
                case COMMA:
                        printf(" COMMA\n");
                        break;
                case LPAREN:
                        printf(" LPAREN\n");
                        break;
                case RPAREN:
                        printf(" RPAREN\n");
                        break;
                case LBRACKET:
                        printf(" LBRACKET\n");
                        break;
                case RBRACKET:
                        printf(" RBRACKET\n");
                        break;
                case LBRACE:
                        printf(" LBRACE\n");
                        break;
                case RBRACE:
                        printf(" RBRACE\n");
                        break;
                case ID:
                        printf(" ID\n");
                        break;
                case NUM:
                        printf(" NUM\n");
                        break;
                case ENDFILE:
                        printf(" ENDFILE\n");
                        break;
                case ERRO:
                        printf(" ERRO\n");
                        break;
                default:
                        break;
        }
}

void print_token(TokenType tokenType, char * lexema, int line) 
{
        if ((tokenType != ENDFILE) && (tokenType != ERRO)) {
                printf("%3d: ", line);
                printf(" Lexema: %-10s | Token:", lexema);
                print_token_type(tokenType);
        } else if (tokenType == ERRO) {
                printf(" \nERRO LEXICO: LINHA: %d lexema = %s\n", line, lexema);
                exit(1);
        } else {
                printf("%3d: ", line);
                printf(" Token:");
                print_token_type(tokenType);
        }
}

TreeNode * new_stmt_node(StmtKind kind, int line)
{
        TreeNode * node = (TreeNode *) malloc(sizeof(TreeNode));

        int childIndex;

        if (node==NULL) {
                printf("Sem memoria, erro na linha %d\n", line);
        } else {
                for (childIndex=0; childIndex<MAXCHILDREN; childIndex++) {
                        node->child[childIndex] = NULL;
                }

                node->kind = (Kind *) malloc(sizeof(Kind));
                node->atribute = (Atribute *) malloc(sizeof(Atribute));

                node->sibling = NULL;
                node->nodeKind = StmtK;
                node->kind->stmt = kind;
                node->line = line;
                node->scope = "global";
        }

        return node;
}

TreeNode * new_exp_node(ExpKind kind, int line)
{
        TreeNode * node = (TreeNode *) malloc(sizeof(TreeNode));

        int childIndex;

        if (node==NULL) {
                printf("Sem memoria, erro na linha %d\n", line);
        } else {
                for (childIndex=0; childIndex<MAXCHILDREN; childIndex++) {
                        node->child[childIndex] = NULL;
                }

                node->kind = (Kind *) malloc(sizeof(Kind));
                node->atribute = (Atribute *) malloc(sizeof(Atribute));

                node->sibling = NULL;
                node->nodeKind = ExpK;
                node->kind->exp = kind;
                node->line = line;
                node->scope = "global";
        }

        return node;
}

static int is_free_node_stmt(TreeNode * node)
{
        return (node->nodeKind == StmtK && (node->kind->stmt == VariableK || node->kind->stmt == FunctionK || node->kind->stmt == CallK));
}

static int is_free_node_exp(TreeNode * node)
{
        return (node->nodeKind == ExpK && (node->kind->exp == ArrayK || node->kind->exp == IdK));
}

static void free_elements_in_node(TreeNode * node)
{
        free(node->kind);
        if (is_free_node_exp(node) || is_free_node_stmt(node)) {
                free(node->atribute->name);
        }
        free(node->atribute);
}

void free_node_tree(TreeNode * node)
{
        int childIndex;

        for (childIndex = 0; childIndex < MAXCHILDREN; childIndex++) {
                if (node->child[childIndex] != NULL) {
                        free_node_tree(node->child[childIndex]);
                }
        }      

        if (node->sibling != NULL) {
                free_node_tree(node->sibling);
        }

        free_elements_in_node(node);
        free(node);
}

char * copy_string(char * string, int line)
{
        int length;
        char * copy;

        if (string == NULL) {
                return NULL;
        }

        length = strlen(string) + 1;
        copy = malloc(length);

        if (copy == NULL) {
                printf("Sem memoria, erro na linha %d\n", line);
        } else {
                strcpy(copy, string);
        }

        return copy;
}

/* 
        Variavel  indentation usada por print_tree para
        armazenar a quantidade corrente de espacoes para tabulacao
 */
static int indentation = 0;

/* 
        macros para aumentar/diminuir a tabulacao
 */
#define INDENT indentation+=2
#define UNINDENT indentation-=2

/* 
        procedimento para imprimir a tabulacao por escopo da impressao
 */
static void print_spaces(void)
{ 
        int i;

        for (i=0;i<indentation;i++)
                printf(" ");
}

void print_tree(TreeNode * node)
{
        int childIndex;
        INDENT;

        while (node != NULL) {
                print_spaces();
                if (node->nodeKind ==  StmtK) {
                        switch (node->kind->stmt) {
                                case IfK:               printf("If\n"); 
                                                        break;
                                case WhileK:            printf("While\n"); 
                                                        break;
                                case AssignK:           printf("Assign\n"); 
                                                        break;
                                case VariableK:         printf("Variable: %s\n", node->atribute->name); 
                                                        break;
                                case FunctionK:         printf("Function: %s\n", node->atribute->name); 
                                                        break;
                                case CallK:             printf("Call: %s\n", node->atribute->name); 
                                                        break;
                                case ReturnK:           printf("Return\n"); 
                                                        break;
                                default:                printf("Tipo StmtNode nao encontrado\n"); 
                                                        break;
                        }
        }
                else if (node->nodeKind ==  ExpK) {
                        switch (node->kind->exp) {
                                case OpK:               printf("Op: "); 
                                                        print_token_type(node->atribute->operator);
                                                        break;
                                case ConstK:            printf("Const: %d\n", node->atribute->value); 
                                                        break;
                                case IdK:               printf("Id: %s\n", node->atribute->name); 
                                                        break;
                                case ArrayK:            printf("Vector: %s \n", node->atribute->name); 
                                                        break;
                                case IndexVector:       printf("Index [%d]\n", node->atribute->value); 
                                                        break;
                                case TypeK:             printf("Type: %s\n", node->atribute->name); 
                                                        break;
                                default:                printf("Tipo ExpNode nao encontrado\n");
                                                        break;
                        }
        }
        else
                printf("Nenhum tipo de no encontrado\n");
        
                for (childIndex=0; childIndex<MAXCHILDREN; childIndex++)
                        print_tree(node->child[childIndex]);
        
                node = node->sibling;
        }

  UNINDENT;
}