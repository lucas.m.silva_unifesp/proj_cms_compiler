#ifndef _UTIL_H_
#define _UTIL_H_

#include "global.h"

/*
        Procedimento que imprime o tipo do token
*/
void print_token_type(TokenType );

/*
        Procedimento que imprime o token
*/
void print_token(TokenType , char * , int);

/*
        Função que cria um novo no do tipo definicao
*/
TreeNode * new_stmt_node(StmtKind, int);

/*
        Função que cria um novo no do tipo expressao
*/
TreeNode * new_exp_node(ExpKind, int);

/*
        Procedimento que libera a memoria de um no
*/
void free_node_tree(TreeNode *);

/*
        Função que copia uma string
*/
char * copy_string(char *, int);

/*
        Função que imprime uma arvore
*/
void print_tree(TreeNode *);

#endif // _UTIL_H_